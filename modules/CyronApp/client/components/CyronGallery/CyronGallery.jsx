import './CyronGallery.css'
import { Component, PropTypes } from 'react'

var {
    Slider
    } = MUI;

const styles = {
    galleryContainer: {
        position: "relative",
        top: "10%",
        overflow: "hidden",
        width: "100%",
        margin: "0 auto"

    },
    gallery: {
        position: "relative",
        overflow: "hidden",
        display: "inline-flex",
        width: "2300px",
        left: "0"
    },
    galleryControls: {
        position: 'relative',
        bottom: '10px',
        margin: "3px 10px",
        padding: "0",
        textAlign: "center",
        fontSize: '15px,',
        color: "#637bac"
    },
    images: {
        position: "relative",
        width: "622px",
        height: "350px",
        margin: "0 1rem"
    },
    arrows: {
        margin: ".8rem",
        color: "blue"
    }
};

/* Parent Component */
export default class CyronGallery extends Component {

    static  propTypes = {
        items: React.PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            left: 0,
            sliderVal: 0.1
        };
    }

    render() {
        return (
            <div className="gallery-container animated fadeIn" style={styles.galleryContainer}>
                <ul className="gallery" ref="gallery" style={styles.gallery}>
                    {this._renderGallery()}
                </ul>
                <div id="slider" style={{margin: "30px"}}>
                    <Slider className="unselectable"
                            name="gallery-slider"
                            style={{color: 'blue', width: "80%", margin: "0 auto", height: "28px"}}
                            onChange={this._onSlide}
                            onDragStart={this._onSlide}
                            onDragEnd={this._onSlide}
                            defaultValue={0}/>
                </div>
            </div>
        );
    }

    _renderGallery = () => {
        return this.props.items.map((item) => {
            return <GalleryItem artistName={item.artistName}
                                className={"gallery-id-" + item._id}
                                imgPath={item.imgPath}
                                pieceName={item.pieceName}
                                key={item._id}/>
        });
    };

    _onSlide = (event, sliderValue) => {
        let $gallery = ReactDOM.findDOMNode(this.refs.gallery),
            _constantWidth = styles.images.width.substring(0, 3),
            _numOfItems = 3;

        this.setState({
            left: -1 * (sliderValue * ( _constantWidth * _numOfItems ))
        });

        $gallery.style.left = this.state.left + 'px';
    };

}


class GalleryItem extends Component {
    render() {
        return (
            <li className="gallery-item" style={styles.images}>
                <div className="gallery-item-name" style={{margin: '8px 0'}}>{this.props.pieceName}</div>
                <img src={this.props.imgPath} className="gallery-img unselectable" style={styles.images}/>
                <h4 className="artist-name" style={{margin: '8px 0'}}>{"By: " + this.props.artistName}</h4>
            </li>
        );
    }
}

