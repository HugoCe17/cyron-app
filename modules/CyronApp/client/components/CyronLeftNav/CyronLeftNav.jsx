import {Component, PropTypes} from 'react'
import { RawThemeCustomizer } from '../../../cyron-classes'

/* material-ui */
import CyronTheme from '../../components/CyronTheme'
import ThemeManager from 'material-ui/lib/styles/theme-manager'
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator'

import { CyronSoundPlayer } from '../../components/CyronSoundPlayer/CyronSoundPlayer.jsx'


const style = {
    leftNav: {backgroundColor: '#34485c', float: 'center', display: 'hidden'},
    icons: {color: '#FFFAE8', top: "5px", left: "20px"},
    avatar: {position: "relative", bottom: "15px", float: "left", right: "15px", cursor: "pointer"},
    card: {position: 'relative', top: '17px', width: '99.777%', borderRadius: '0', backgroundColor: "#FFFAE8"},
    cardHeader: {position: "relative", top: "3px", float: "right"},
    menuItem: {color: '#FFFAE8', marginTop: "1px"},
    snackbar: {}

};

let ThemeCustomizer = new RawThemeCustomizer(CyronTheme),
    newTheme = ThemeCustomizer.customizePalette(CyronTheme.palette, {
        canvasColor: '#FFFAE8',
        textColor: "#34485c",
        alternateTextColor: "#FFFAE8"
    });

let { Avatar, Card, CardHeader, LeftNav, MenuItem, Snackbar } = MUI;

@ThemeDecorator(ThemeManager.getMuiTheme(newTheme))
export default class CyronLeftNav extends Component {
    static propTypes = {
        open: PropTypes.bool.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            isMobile: typeof window !== 'undefined' && window.innerWidth < 768,
            showMP: false,
            playerAnimation: '',
            display: '',
            active: {}
        };
    }

    componentWillMount() {
        if (!this.state.isMobile) {
            this.setState({
                display: 'none'
            })
        }
    }

    render() {
        let avatarUrl = "https://avatars.githubusercontent.com/u/1256596?v=3";

        let showPlayerOK =
            !this.state.isMobile
                ?
                <div className={"animated " + this.state.playerAnimation}
                     style={{display: this.state.display}}>
                    <CyronSoundPlayer/>
                </div>
                :
                null;

        return (
            <div>
                <LeftNav open={this.props.open}
                         style={style.leftNav}
                         width={220}
                         key={0}>
                    <Card style={style.card}>
                        <CardHeader title="Hugo Cedano"
                                    subtitle="Fullstack Developer"
                                    avatar={null}
                                    style={style.cardHeader}
                                    zDepth={2}>
                            <Avatar size={65}
                                    style={style.avatar}
                                    src={avatarUrl}
                                    onClick={ this._handleOpenSnackbar}/>
                        </CardHeader>
                    </Card>
                    <br/>
                    {this._createMenu() }

                    <MenuItem leftIcon={<i className="fa fa-music" style={style.icons}/>}
                              primaryText="Music"
                              style={_.extend({}, style.menuItem, this.state.active)}
                              onTouchTap={this._showPlayer}
                              rippleColor={"#FFFAE8"}
                              className="animated fadeInLeft"/>

                    {/*Show player only if LeftNav is showing*/}
                    {showPlayerOK}
                </LeftNav>

                <Snackbar open={this.state.open}
                          message="Check my Bitbucket?"
                          action="Go"
                          onActionTouchTap={this._routeToGo}
                          autoHideDuration={3000}
                          onRequestClose={this._handleRequestClose}/>
            </div>

        )
    }

    _showPlayer = ()=> {
        if (this.state.showMP === false) {
            this.setState({
                showMP: true,
                playerAnimation: 'animated fadeInLeft',
                display: 'block',
                active: {borderRight: "1px inset solid  #grey", backgroundColor: '#1F2B3E'}
            })
        } else {
            this.setState({showMP: false, playerAnimation: 'animated fadeOutLeft', active: {}})
        }
    };

    _handleOpenSnackbar = () => {
        this.setState({
            open: !this.state.open
        })
    };

    _handleRequestClose = () => {
        this.setState({
            open: false
        });
    };

    _routeToGo = () => {
        if (typeof window !== 'undefined')
            window.location.href = 'http://bitbucket.com/HugoCe17'
    };

    _createMenu = () => {
        let menuOptionsAndIcons = {
                'About me': <i className="fa fa-male" style={style.icons}/>,
                'Skills': <i className="fa fa-certificate" style={style.icons}/>,
                'Interests': <i className="fa fa-cogs" style={style.icons}/>
            },
            menu = [];

        function _routeHandler() {

            switch (this) {
                case 'About me':
                    return function () {
                        if (typeof window !== 'undefined')
                            window.location.href = 'http://' + window.location.host + '/about_me';
                    };
                    break;
                case 'Skills':
                    return function () {
                        if (typeof window !== 'undefined')
                            window.location.href = 'http://' + window.location.host + '/skills';
                    };
                    break;
                case 'Interests':
                    return function () {
                        if (typeof window !== 'undefined')
                            window.location.href = 'http://' + window.location.host + '/interests';
                    };
                    break;
                default:
                    break;
            }
        }

        for (var prop in menuOptionsAndIcons) {
            if (menuOptionsAndIcons.hasOwnProperty(prop)) {
                menu.push(<MenuItem leftIcon={menuOptionsAndIcons[prop]}
                                    primaryText={ prop }
                                    style={style.menuItem}
                                    onTouchTap={_routeHandler.call(prop)}
                                    key={prop}
                                    className="animated fadeInLeft"/>)
            }
        }

        return menu
    };

}