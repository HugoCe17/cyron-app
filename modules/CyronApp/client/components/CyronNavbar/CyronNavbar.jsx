import { Component, PropTypes } from 'react'
import {Link, browserHistory} from 'react-router'
import ReactMixin from 'react-mixin'

import {CyronSoundPlayerMobile }from '../../components/CyronSoundPlayer/CyronSoundPlayer.jsx'

import ThemeManager from 'material-ui/lib/styles/theme-manager'
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator'

import CyronTheme from '../CyronTheme'
import { RawThemeCustomizer }from '../../../cyron-classes'

import MoreVertIcon from 'material-ui/lib/svg-icons/navigation/more-vert';

let {
    IconMenu,
    MenuItem,
    IconButton,
    AppBar
    } = MUI;

const style = {
    icons: {color: '#FFFAE8', top: "5px", left: "20px"},
    menuItem: {color: '#FFFAE8', marginTop: "1px"},
    title: {
        cursor: 'pointer'
    },
    musicIcon: {
        position: 'absolute',
        color: '#FFFAE8',
        'left': '194px',
        fontSize: '32px',
        top: '17px'
    }
};

//customizePalette(paletteObj, colorK, colorV)
let ThemeCustomizer = new RawThemeCustomizer(CyronTheme);
var newTheme = ThemeCustomizer.customizePalette(CyronTheme.palette, {
    alternateTextColor: '#FFFAE8',
    canvasColor: "#34485c"
});


//const registrationBtn = (
//    <div className="row">
//        <FlatButton id="registration-btn"
//                    className="flat-btn"
//                    label="Register"
//                    primary={true}
//                    containerElement={<Link to="/registration"/>}
//                    linkButton={true}
//                    style={{position: 'right', left: '78px'}}/>
//    </div>
//);

@ThemeDecorator(ThemeManager.getMuiTheme(newTheme))
export default class CyronNavbar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            depth: 0,
            title: 'Cyron',
            isMobile: typeof window !== 'undefined' && window.innerWidth < 768,
            navbarColor: 'transparent',
            navbarHeader: null,
            fontSize: '20px',
            barWidth: typeof window !== 'undefined' ? window.innerWidth : '1200',
            isOpen: false,
            playerAnimation: '',
            display: 'none',
            showMP: false,
            active: {},
            musicIcon: <i className="animated fadeIn fa fa-volume-off"
                          style={_.extend({}, style.musicIcon, {color: "transparent"})}/>,
            navbarPos: window !== 'undefined' && window.innerWidth <= 320 ?
            ( window.innerHeight - window.innerHeight * .13) - 7 :
                ( window.innerHeight - window.innerHeight * .13)
        };

    }

    componentDidMount() {
        if (window) {
            window.addEventListener('scroll', this.handleScroll);
            window.addEventListener('resize', this._adaptDimension)
        }
    }

    componentWillUnmount() {
        if (window) {
            window.removeEventListener('scroll', this.handleScroll);
            window.removeEventListener('resize', this._adaptDimension)
        }
    }

    render() {
        let showPlayerOK =
            this.state.isMobile
                ?
                <div className={this.state.playerAnimation}
                     ref="bottomPlayer"
                     style={{
                         position: "fixed", display: this.state.display,zIndex: '10', top: this.state.navbarPos + 'px',
                         width: window !== 'undefined' ? window.innerWidth : null
                     }}>
                    <CyronSoundPlayerMobile/>
                </div>
                :
                null;


        return (
            <div>
                <AppBar ref="appbar"
                        title={ this.state.title }
                        iconElementRight={<IconMenu iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                                    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                                    targetOrigin={{horizontal: 'right', vertical: 'top'}}>

            {this._createMenu()}

            <MenuItem leftIcon={<i className="fa fa-music animated fadeInLeft" style={style.icons}/>}
                      primaryText="Music"
                      style={_.extend({}, style.menuItem, this.state.active)}
                      onTouchTap={this._showPlayer}
                      className="animated fadeInDown"/>
        </IconMenu>}
                        iconElementLeft={this.state.musicIcon}
                        showMenuIconButton={true}
                        onTitleTouchTap={this._onTouchTap}
                        zDepth={ this.state.depth }
                        style={this._navbarStyle()}
                        className="appbar"/>

                {/*Show player iff on mobile*/}
                {showPlayerOK}

            </div>
        );
    }

    _showPlayer = () => {
        if (this.state.showMP === false) {
            this.setState({
                showMP: true,
                display: 'block',
                playerAnimation: 'animated fadeInUp',
                active: {borderRight: "1px inset solid  #grey", backgroundColor: '#1F2B3E'},
                musicIcon: <i className="animated fadeIn fa fa-volume-up" style={style.musicIcon}/>
            });
        } else {
            this.setState({
                showMP: false,
                playerAnimation: 'animated fadeOutDown',
                active: {},
                musicIcon: <i className="animated fadeOut fa fa-volume-off" style={style.musicIcon}/>
            })
        }
    };


    _adaptDimension = () => {
        if (window.innerWidth < 768) {
            this.setState({
                isMobile: true,
                open: false,
                barWidth: window.innerWidth
            });
            document.body.style.paddingLeft = '0';
        } else {
            this.setState({
                isMobile: false,
                open: true,
                barWidth: null
            });
            document.body.style.paddingLeft = '16rem';
        }
    };

    _createMenu = () => {
        let menuOptionsAndIcons = {
                'About me': <i className="fa fa-male animated fadeInLeft" style={style.icons}/>,
                'Skills': <i className="fa fa-certificate animated fadeInLeft" style={style.icons}/>,
                'Interests': <i className="fa fa-cogs animated fadeInLeft" style={style.icons}/>
            },
            menu = [];

        function _routeHandler() {
            switch (this) {
                case 'About me':
                    return function () {
                        if (typeof window !== 'undefined')
                            window.location.href = '' + 'http://' + window.location.host + '/about_me';
                    };
                    break;
                case 'Skills':
                    return function () {
                        if (typeof window !== 'undefined')
                            window.location.href = '' + 'http://' + window.location.host + '/skills';
                    };
                    break;
                case 'Interests':
                    return function () {
                        if (typeof window !== 'undefined')
                            window.location.href = '' + 'http://' + window.location.host + '/interests';
                    };
                    break;
                default:
                    break;
            }
        }

        for (var prop in menuOptionsAndIcons) {
            if (menuOptionsAndIcons.hasOwnProperty(prop)) {
                menu.push(<MenuItem leftIcon={menuOptionsAndIcons[prop]}
                                    primaryText={ prop }
                                    style={style.menuItem}
                                    onTouchTap={_routeHandler.call(prop)}
                                    key={prop}
                                    rippleColor={"#FFFAE8"}
                                    className="animated fadeInDown"/>)
            }
        }

        return menu
    };

    _onTouchTap = () => {
        if (typeof window !== 'undefined') window.scrollTo(0, 0);
    };


    handleScroll = (e) => {
        let scrollTop = e.srcElement.body.scrollTop,
            itemTranslate = window !== 'undefined' ? scrollTop + (window.innerHeight - window.innerHeight * .13 ) : null;

        if (this.state.showMP === true) {
            ReactDOM.findDOMNode(this.refs.bottomPlayer).style.top = itemTranslate + 'px';
            if (window !== 'undefined' && window.innerWidth <= 320) {
                ReactDOM.findDOMNode(this.refs.bottomPlayer).style.top = (itemTranslate - 7) + 'px';
            }
        }

        if (scrollTop <= 75) {
            this.setState({depth: 0});
        } else {
            this.setState({depth: 2});
        }

        if (scrollTop >= 336) {
            if (Meteor.user()) {
                this.setState({title: "Welcome, " + Meteor.user().profile.firstName});
                //this.setState({title: "Welcome, " + this.props.user.profile.firstName})
            } else {
                this.setState({title: "Cyron"})
            }
        } else {
            this.setState({title: "Cyron"})
        }

        if (scrollTop >= 600) {
            // change style on here
            this.setState({
                navbarColor: "#30485e"
            })
        } else {
            this.setState({
                navbarColor: 'transparent'
            })
        }

    };

    _navbarStyle() {
        let navBarStyle = {
            width: this.state.barWidth,
            position: 'fixed',
            backgroundColor: this.state.navbarColor
        };
        return _.extend(style.title, navBarStyle);
    }
}



