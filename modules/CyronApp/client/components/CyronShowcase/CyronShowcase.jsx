import './CyronShowcase.css'
import { Component, PropTypes } from 'react'

import cssHtmlBootstrapImg from './logos/css3-html5-bootstrap.png'
import nodejsImg from './logos/nodejs.png'
import sassImg from './logos/sass-logo.png'
import webpackImg from './logos/webpack.png'
import reactjsImg from './logos/reactjs.svg'
import meteorImg from './logos/meteor-logo.png'

const styles = {
    meteorImgStyle: {position: "relative", width: '10rem', height: '100%', top: '18px'}
};

// Showcase a set of BrandImages
export default class CyronShowcase extends Component {

    static propTypes = {};

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="showcase-tech-container">
                <ul className="brand-logos col-md-11 col-lg-10 col-lg-offset-2" id="logos">
                    <li><img src={cssHtmlBootstrapImg}/></li>
                    <li><img src={nodejsImg}/></li>
                    <li><img src={meteorImg}
                             style={styles.meteorImgStyle}/></li>
                    <li><img src={sassImg}/></li>
                    <li><img src={webpackImg}/></li>
                    <li><img src={reactjsImg}/></li>
                </ul>
            </div>
        )
    }

}
