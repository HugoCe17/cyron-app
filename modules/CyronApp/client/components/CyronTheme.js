import Colors from 'material-ui/lib/styles/colors';
import ColorManipulator from 'material-ui/lib/utils/color-manipulator';
import Spacing from 'material-ui/lib/styles/spacing';
import zIndex from 'material-ui/lib/styles/zIndex';

export default {
    spacing: Spacing,
    zIndex: zIndex,
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: Colors.blueGrey500,
        primary2Color: Colors.grey300,
        primary3Color: Colors.lightBlack,
        accent1Color: Colors.blueGrey500,
        accent2Color: Colors.grey100,
        accent3Color: Colors.grey500,
        textColor: '#FFFAE8',
        alternateTextColor: '#FFFAE8',
        canvasColor: 'transparent',
        borderColor: '#FFFAE8',
        disabledColor: ColorManipulator.fade(Colors.white, 0.3),
        pickerHeaderColor: '#FFFAE8'
    }
};
