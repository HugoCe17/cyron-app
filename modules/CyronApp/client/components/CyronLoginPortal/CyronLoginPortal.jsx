import './CyronLoginPortal.css'
import {Component, PropTypes} from 'react'
import ReactMixin from 'react-mixin'
import ReactDOM from 'react-dom'
import {Link} from 'react-router'
import bgImg from '../../img/mountain-bg.jpg'

import CyronTheme from '../CyronTheme'
import ThemeManager from 'material-ui/lib/styles/theme-manager'
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator'
import LinkedStateMixin from 'react/lib/LinkedStateMixin'

import { RawThemeCustomizer } from '../../../cyron-classes'

let {
    FlatButton,
    TextField
    } = MUI;

const style = {
    loginPortal: {
        position: "relative",
        top: "35%",
        width: "300px",
        margin: "0 auto",
        border: "solid 1px",
        borderRadius: "0",
        color: '#FFFAE8',
        padding: "22px"
    },
    fullBG: {
        background: "url(" + bgImg + ")",
        backgroundPosition: "50% 0",
        backgroundSize: "cover",
        height: "100%",
        position: "fixed",
        top: "0",
        bottom: "0",
        left: "0",
        right: "0",
        width: "100%"
    }
};
let ThemeCustomizer = new RawThemeCustomizer(CyronTheme);
let newTheme = ThemeCustomizer.customizePalette(CyronTheme.palette, {
    textColor: '#FFFAE8',
    alternateTextColor: '#FFFAE8',
    primary1Color: '#FFFAE8',
    borderColor: '#FFFAE8'
});

@ThemeDecorator(ThemeManager.getMuiTheme(newTheme))
@ReactMixin.decorate(LinkedStateMixin)
export default class CyronLoginPortal extends Component {

    static propTypes = {
        history: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            usernameError: null,
            passwordError: null,
            errorSigningIn: false
        };
    }

    componentDidMount() {
        Meteor.setTimeout(()=> {
            if (Meteor.user()) this.props.history.push(`/welcome/${Meteor.user().username}`)
        }, 600);
    }

    render() {
        let username = this.linkState('username'),
            password = this.linkState('password');
        return (
            <div className="login-container container " style={style.fullBG}>
                <div className="login-portal" style={style.loginPortal}>
                    <h3 style={{textAlign: "center"}}>Login</h3>
                    <form onSubmit={this._handleSubmit} onChange={this.clearErrors}>
                        <div className="row field-container">
                            <TextField
                                valueLink={username}
                                ref="username"
                                name="username"
                                required={true}
                                type="text"
                                errorText={this.state.usernameError}
                                floatingLabelText="Username"/>
                            <TextField
                                valueLink={password}
                                ref="password"
                                name="password"
                                required={true}
                                type="password"
                                floatingLabelText="Password"
                                errorText={this.state.passwordError}/>
                            <div className="row flat-btns">
                                <FlatButton className="flat-btn"
                                            type="submit"
                                            label="Sign-in"
                                            linkButton={false}
                                            primary={false}/>

                                <FlatButton className="flat-btn"
                                            label="Guest"
                                            primary={false}
                                            onClick={ this._routeGuest }
                                            onTouchStart={ this._routeGuest }
                                            linkButton={false}/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }

    _routeGuest = () => {
        this.props.history.push('/welcome')
    };

    _handleSubmit = (e) => {
        e.preventDefault();
        let username = this.state.username.trim().toLowerCase(),
            password = this.state.password;

        // Sign user in
        this._login(username, password)
    };

    _login(user, password) {
        /* Clean this up with ternary */
        if (user && password) Meteor.loginWithPassword(user, password, err => {
            if (err) {
                console.log("There was an error: " + err.reason);
                if (err.reason === "Incorrect password") {
                    // reset userError if user is correct, but password is wrong
                    if (err.reason !== "Incorrect Password") {
                        this.setState({
                            usernameError: null
                        })
                    }
                    this.setState({passwordError: 'oops... password error'});
                } else if (err.reason !== "Incorrect Password") {
                    this.setState({usernameError: 'oops... username error'});
                }
            } else {
                this.props.history.push(`/welcome/${Meteor.user().username}`);
            }
        })
    }

};