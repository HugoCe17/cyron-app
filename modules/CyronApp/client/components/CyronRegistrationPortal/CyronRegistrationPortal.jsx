import './CyronRegistrationPortal.css'
import {Component, PropTypes} from 'react'
import ReactMixin from 'react-mixin'
import ReactDOM from 'react-dom'
import { Link, History } from 'react-router'
import bgImg from '../../img/mountain-bg.jpg'
import CyronTheme from '../CyronTheme'

// User helper class
import { _UserHelper } from '../../../cyron-classes'

import LinkedStateMixin from 'react/lib/LinkedStateMixin'

import ThemeManager from 'material-ui/lib/styles/theme-manager'
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator'

let {
    FlatButton,
    TextField
    } = MUI;

/* Registration form */
@ThemeDecorator(ThemeManager.getMuiTheme(CyronTheme))
@ReactMixin.decorate(LinkedStateMixin, History)
export default class CyronRegistrationPortal extends Component {

    static contextTypes = {
        history: PropTypes.object.isRequired
    };

    static propTypes = {};

    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            phone: '',
            password: '',
            confirmPassword: '',
            errorStyle: {}
        };

    }

    handleSubmit = (event) => {
        event.preventDefault();
        // Make sure password and confirmPassword match
        // If they match, createUser, if not... don't submit form and print error
        if (this.state.password !== this.state.confirmPassword) {
            this.setState({
                passwordErrorMsg: 'Passwords do not match!',
                errorStyle: _.extend(this.state.errorStyle, {color: 'red'})
            })
        } else {
            this.setState({
                passwordErrorMsg: "Matched",
                errorStyle: _.extend(this.state.errorStyle, {color: 'green'})
            });

            // Prepare user bundle
            let username = this.state.email.trim().toLowerCase().split('@')[0],
                email = this.state.email.trim().toLowerCase(),
                password = this.state.password,
                profile = {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    name: this.state.firstName + ' ' + this.state.lastName,
                    phone: this.state.phone
                };

            // constructor(username, email, password, profile)
            let registrant = new _UserHelper(username, email, password, profile);

            // Finally, send user Obj to create account using Meteor methods
            this._processAccountCreation(registrant.getUserOptions())
        }

    };

    render() {
        let firstName = this.linkState('firstName'),
            lastName = this.linkState('lastName'),
            email = this.linkState('email'),
            phone = this.linkState('phone'),
            password = this.linkState('password'),
            confirmPassword = this.linkState('confirmPassword');

        return (
            <div className="registration-container container" ref="registrationPage" style={this.getFullBGStyle()}>
                <div className="registration-portal" style={this.getLoginPortalStyle()}>
                    <h3 style={{textAlign: "center"}}>Registration</h3>
                    <form onSubmit={this.handleSubmit}>
                        <div className="row field-container" style={ {marginLeft: '40px'} }>
                            <TextField
                                valueLink={ firstName }
                                ref="firstName"
                                name="firstName"
                                required={true}
                                type="text"
                                floatingLabelText="First Name"/>
                            <TextField
                                valueLink={ lastName }
                                ref="lastName"
                                name="lastName"
                                required={true}
                                onChange={this._handleFirstName}
                                type="text"
                                floatingLabelText="Last Name"/>
                            <TextField
                                valueLink={ email }
                                ref="email"
                                name="email"
                                required={true}
                                type="email"
                                floatingLabelText="Email"/>
                            <TextField
                                valueLink={ phone }
                                ref="phone"
                                name="phone"
                                required={true}
                                type="tel"
                                floatingLabelText="Phone"/>
                            <TextField
                                valueLink={ password }
                                ref="password"
                                name="password"
                                required={true}
                                type="password"
                                floatingLabelText="Password"
                                errorText={this.state.passwordErrorMsg}
                                errorStyle={this.state.errorStyle}/>
                            <TextField
                                valueLink={ confirmPassword }
                                ref="confirmPassword"
                                name="confirmPassword"
                                required={true}
                                type="password"
                                floatingLabelText="Confirm Password"
                                errorText={this.state.passwordErrorMsg}
                                errorStyle={this.state.errorStyle}/>
                            <div className="row flat-btns">
                                <FlatButton className="flat-btn"
                                            type="submit"
                                            label="Submit"
                                            linkButton={false}
                                            primary={true}
                                            style={{position: 'relative', bottom: '5px', left: '30px'}}/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }

    _processAccountCreation(userObj) {
        if (userObj) {
            Accounts.createUser(userObj, err => {
                if (err) {
                    console.log(err.reason)
                } else {
                    console.log("User creation was successful" + '\n' + "Login user in");
                    this._processLogin(userObj)
                }
            });
        }
    }

    _processLogin(userObj) {
        // Login using password
        Meteor.loginWithPassword(userObj.username, userObj.password, err => {
            if (err) {
                console.log(err)
            } else {
                this.context.history.pushState(null, `/welcome/${Meteor.user().username}`);
            }
        })
    }

    getLoginPortalStyle() {
        return {
            position: "relative", top: "20%", width: "380px",
            margin: "0 auto",
            border: "solid 1px",
            color: "white",
            borderRadius: "0",
            padding: "22px"
        }
    }

    getFullBGStyle() {
        return {
            background: "url(" + bgImg + ")",
            backgroundPosition: "50% 0",
            backgroundSize: "cover",
            height: "100%",
            position: "fixed",
            top: "0",
            bottom: "0",
            left: "0",
            right: "0",
            width: "100%"
        }
    }

};