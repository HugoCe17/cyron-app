import  { Component, PropTypes } from 'react'
export default class CyronError extends Component {
    render() {
        return (
            <div className="jumbotron">
                <h1 style={{textAlign: 'center'}}>Error 404</h1>
                <p style={{textAlign: 'center'}}>Seems like you're trying to access a page we don't serve...</p>
            </div>
        )
    }
}