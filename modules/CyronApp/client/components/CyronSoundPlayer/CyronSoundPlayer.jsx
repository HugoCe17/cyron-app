/* TO-DO */
// Extend Player for navBar or leftNav settings through props
// playerStyle={desktop||mobile}

import {Component, PropTypes} from 'react'
import ClassNames from 'classnames'
import { PlayButton, NextButton, PrevButton, Progress, Timer } from 'react-soundplayer/components';
import { SoundPlayerContainer } from 'react-soundplayer/addons';

const clientId = '370c9d3047714b6c921a241fe0450d3d',
    resolveUrl = 'https://soundcloud.com/vahlk/sets/originalfun';

const style = {
    headers: {color: '#FFFAE8', position: "relative", margin: "0 auto"}
};

class Player extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeIndex: 0,
            playPauseIcon: 'fa fa-play'
        }
    }

    componentDidMount() {
        let {soundCloudAudio} = this.props;

        soundCloudAudio.on('ended', () => {

            this.setState({
                playPauseIcon: 'fa fa-pause',
                activeIndex: this.state.activeIndex + 1
            });

            soundCloudAudio.next();

        })
    }

    render() {
        let { playlist } = this.props;

        return (
            <div>
                <h3 style={style.headers}>
                    { playlist ? playlist.user.username : 'Loading...' }
                </h3>
                <h1 style={style.headers}>
                    { playlist ? playlist.tracks[this.state.activeIndex].title : 'Loading...' }
                </h1>
                <Timer  {...this.props}/>

                <button
                    className="sb-btn fa fa-step-backward"
                    onClick={this.prevIndex}/>

                <button
                    className={"sb-btn " + this.state.playPauseIcon}
                    onClick={this.playCurrentIndex}/>

                <button
                    className="sb-btn fa fa-step-forward"
                    onClick={this.nextIndex}/>

            </div>
        );

    }

    nextIndex = () => {
        let {playlist, soundCloudAudio} = this.props;
        let {activeIndex} = this.state;
        if (activeIndex >= playlist.tracks.length - 1) {
            return;
        }
        if (activeIndex || activeIndex === 0) {
            this.setState({activeIndex: ++activeIndex, playPauseIcon: 'fa fa-pause'});
            soundCloudAudio.next();

        }

    };

    playCurrentIndex = () => {
        let {soundCloudAudio, playing} = this.props;
        let {activeIndex} = this.state;
        if (playing) {
            soundCloudAudio.pause();
            this.setState({playPauseIcon: 'fa fa-play'})
        } else {
            soundCloudAudio.play({playlistIndex: activeIndex});
            this.setState({playPauseIcon: 'fa fa-pause'});
        }
    };

    prevIndex = () => {
        if (this.state.activeIndex <= 0) {
            return;
        }
        if (this.state.activeIndex || this.state.activeIndex === 0) {
            this.setState({activeIndex: --this.state.activeIndex});
            this.props.soundCloudAudio.previous();
            this.setState({playPauseIcon: 'fa fa-pause'});
        }
    };


}

export class CyronSoundPlayer extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="sp-container-lg">
                <SoundPlayerContainer resolveUrl={resolveUrl}
                                      clientId={clientId}>
                    <Player />
                </SoundPlayerContainer>
            </div>
        )
    }

}

export class CyronSoundPlayerMobile extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="sp-container-sm">
                <SoundPlayerContainer resolveUrl={resolveUrl}
                                      clientId={clientId}>
                    <Player />
                </SoundPlayerContainer>
            </div>
        )
    }
}