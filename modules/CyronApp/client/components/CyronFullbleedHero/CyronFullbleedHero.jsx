import { Component, PropTypes } from 'react'

export default class CyronFullbleedHero extends Component {
    static propTypes = {
        backgroundColor: PropTypes.string,
        heading: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            winWidth: typeof window !== 'undefined' ? window.innerWidth : '',
            winHeight: typeof window !== 'undefined' ? window.innerHeight : '',
            isMobile: typeof window !== 'undefined' && window.innerHeight < 768,
            animation: ''
        };
    }

    componentWillMount() {
        if (!this.state.isMobile) {
            this.setState({
                animation: 'animated flipInY'
            })
        } else {
            this.setState({
                animation: 'animated fadeInUpBig'
            })
        }
    }

    componentDidMount() {
        if (typeof window !== 'undefined') {
            this.setState({
                winHeight: window.innerHeight
            });
            window.addEventListener('resize', this._handleResize);

        }
    }

    componentWillUnmount() {
        if (window) {
            window.removeEventListener('resize', this._handleResize)
        }
    }

    render() {
        return (
            <div>
                <div className={"fullbleed container " + this.state.animation}
                     ref="fullbleedContainer"
                     style={this._getContainerStyle()}>
                </div>
                <Article heading={this.props.heading}
                         body={this.props.body}
                         position={this.props.position}/>

            </div>
        );
    }

    _handleResize = () => {
        if (typeof window !== 'undefined') {
            this.setState({
                winWidth: window.innerWidth
            });
        }
    };

    _getContainerStyle() {
        return {
            background: 'url(' + this.props.imgPath + ')',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: "50% 0",
            height: '' + (this.state.winHeight + 1) + 'px',
            minHeight: '768px',
            width: '' + this.state.winWidth + 'px',
            display: 'block',
            paddingRight: '0',
            paddingLeft: '0',
            position: 'relative',
            zIndex: '-1'
        }

    }
}

class Article extends Component {
    static propTypes = {
        position: PropTypes.string.isRequired,
        heading: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            isMobile: typeof window !== 'undefined' && window.innerWidth < 500,
            animation: ''
        }
    }

    componentDidMount() {
        if (!this.state.isMobile) {
            this.setState({
                animation: 'animated fadeInUp'
            })
        } else {
            this.setState({
                animation: 'animated fadeInUpBig'
            })
        }
    }

    render() {
        return (
            <div className={this.state.animation}
                 id="fullbleed-text"
                 style={this._getPosition(this.props.position)}>
                <h1 id="fullbleed-h1" style={{color: "#FFFAE8", fontSize: '20px'}}>{this.props.heading}</h1>
                <p id="fullbleed-body" style={{color: "#FFFAE8", fontSize: '16px'}}>{this.props.body}</p>
            </div>

        )
    }

    _getPosition() {
        switch (this.props.position) {
            case "topCenter":
                return {
                    textAlign: "center",
                    bottom: "610px",
                    position: "relative"
                };
            case "topRight":
                return {
                    position: 'relative',
                    textAlign: "right",
                    bottom: '40rem',
                    right: '6rem'
                };
            case "topLeft":
                return {
                    position: 'relative',
                    textAlign: "left",
                    bottom: '40rem',
                    left: '6rem'
                };
            case "centerCenter":
                if (this.state.isMobile) {
                    return {
                        position: 'relative',
                        textAlign: "center",
                        bottom: '28rem'
                    };
                }
                return {
                    position: 'relative',
                    textAlign: "center",
                    bottom: '33rem'
                };
            case "centerLeft":
                return {
                    position: 'relative',
                    textAlign: "left",
                    bottom: '27rem',
                    left: '6rem'
                };
            case "centerRight":
                return {
                    position: 'relative',
                    textAlign: "right",
                    bottom: '27rem',
                    right: '6rem'
                };
            case "bottomCenter":
                return {
                    textAlign: "center",
                    bottom: "6rem",
                    position: "relative"
                };
            case "bottomLeft":
                return {
                    position: 'relative',
                    textAlign: "left",
                    bottom: '6rem',
                    left: '6rem'
                };
            case "bottomRight":
                return {
                    position: 'relative',
                    textAlign: 'right',
                    bottom: '6rem',
                    right: '6rem'
                };
            default:
                return {
                    position: 'relative',
                    textAlign: "center",
                    bottom: '26rem'
                }
        }
    }
}
