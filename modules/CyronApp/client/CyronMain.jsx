import { Component, PropTypes } from 'react'
import ReactMixin from 'react-mixin'

/***-Modules-************************************
 *   Navbar OR LeftNav
 *   FullBleed
 *   Gallery
 ************************************************/
import CyronNavbar from './components/CyronNavbar/CyronNavbar.jsx'
import CyronLeftNav from './components/CyronLeftNav/CyronLeftNav.jsx'
import CyronFullbleedHero from './components/CyronFullbleedHero/CyronFullbleedHero.jsx'
import CyronGallery from './components/CyronGallery/CyronGallery.jsx'
import CyronShowcase from './components/CyronShowcase/CyronShowcase.jsx'
//import CyronAboutMe from './components/CyronAboutMe/CyronAboutMe.jsx'
//import CyronSoundPlayer from './components/CyronSoundPlayer/CyronSoundPlayer.jsx'


import bgImg from './img/mountain-bg.jpg';

import { RawThemeCustomizer } from '../cyron-classes'

import CyronTheme from './components/CyronTheme'
import ThemeManager from 'material-ui/lib/styles/theme-manager'
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator'

let ThemeCustomizer = new RawThemeCustomizer(CyronTheme);
var newTheme = ThemeCustomizer.customizePalette(CyronTheme.palette, {
    canvasColor: 'white',
    alternateTextColor: 'white'
});

@ThemeDecorator(ThemeManager.getMuiTheme(newTheme))
export default class CyronMain extends Component {
    static propTypes = {
        history: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            isMobile: false,
            open: typeof window !== 'undefined' && window.innerWidth >= 768
        };
    }

    componentDidMount() {
        if (typeof window !== 'undefined') {
            this._adaptDimensions();

            window.addEventListener('resize', this._adaptDimensions);
            window.setTimeout(()=> {
                if (Meteor.user()) this.props.history.push(`/welcome/${Meteor.user().username}`)
            }, 600);
        }
    }

    componentWillUnmount() {
        if (window) window.removeEventListener('resize', this._adaptDimensions)
    }

    render() {
        let items = [
            {
                _id: 1,
                artistName: "H. Cedano",
                pieceName: "El River",
                imgPath: require('./img/River.jpg')
            }, {
                _id: 2,
                artistName: "L. Cubano",
                pieceName: "El Miami",
                imgPath: require("./img/miami-bg.jpg")
            }, {
                _id: 3,
                artistName: "Quin",
                pieceName: "Ascension",
                imgPath: "http://tinyurl.com/oj8ygs9"
            }
        ];

        let heading = "Cyron",
        // if user is logged in, let's lets show them love!
            body = Meteor.user() ? "Welcome, " + Meteor.user().username : "Currently in construction",
            Nav = this.state.isMobile ? <CyronNavbar/> : <CyronLeftNav open={this.state.open}/>;

        return (
            <div>
                {Nav}
                <CyronFullbleedHero imgPath={bgImg}
                                    heading={heading}
                                    body={body}
                                    position="centerCenter"
                                    backgroundColor="transparent"/>
                <CyronShowcase/>
                <CyronGallery items={items}/>
            </div>
        )
    }

    _adaptDimensions = () => {
        if (window.innerWidth < 768) {
            document.body.style.paddingLeft = '0';
            this.setState({
                isMobile: true,
                open: false
            });
        } else {
            document.body.style.paddingLeft = '13rem';
            this.setState({
                isMobile: false,
                open: true
            });
        }
    };

}