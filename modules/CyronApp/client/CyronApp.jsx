import { Component, PropTypes } from 'react';
import ReactMixin from 'react-mixin'
import Helmet from 'react-helmet'


injectTapEventPlugin();

export default class CyronApp extends Component {
    static propTypes = {
        children: PropTypes.any.isRequired
    };

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <Helmet
                    title="Cyron; A Portfolio"
                    meta={[
                        { name: "apple-mobile-web-app-capable", content:"yes"},
                        { name: 'description', content: 'Cyron\'s Landing Page'},
                        { name:"apple-mobile-web-app-status-bar-style", content: "black-translucent"},
                        { name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0'}
                    ]}
                    link={[
                        {href: "https://fonts.googleapis.com/icon?family=Material+Icons", rel: "stylesheet"},
                        {"rel":'stylesheet', "href":'https://fonts.googleapis.com/css?family=Roboto', "type":'text/css'},
                        {"rel": "stylesheet", "href": "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"}
                    ]}
                />
                {this.props.children}

            </div>
        );
    }
}