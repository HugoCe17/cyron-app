import { browserHistory, Route, IndexRoute} from 'react-router'

import CyronApp from './CyronApp'
import CyronMain from './CyronMain'
import CyronLoginPortal from './components/CyronLoginPortal/CyronLoginPortal.jsx'
import CyronRegistrationPortal from './components/CyronRegistrationPortal/CyronRegistrationPortal.jsx'

import CyronAboutMe from './components/CyronAboutMe/CyronAboutMe.jsx'
import CyronError from './components/CyronError/CyronError.jsx'


export default (
    <Route path="/" component={CyronApp} history={browserHistory}>
        <IndexRoute component={CyronLoginPortal}/>
        <Route path="welcome" component={CyronMain}/>
        <Route path="/welcome/:username" component={CyronMain}/>
        <Route path="/about_me" component={CyronAboutMe}/>
        <Route path="registration" component={CyronRegistrationPortal}/>
        <Route path="*" component={CyronError}/>
    </Route>
);
