/************************************************************************************
 * User Helper class to be used in conjunction with Accounts-password meteor module
 ************************************************************************************/
export class _UserHelper {
    constructor(username, email, password, profile) {
        if (username && email && password && profile) {
            this.username = username;
            this.email = email;
            this.password = password;
            this.profile = profile;
        } else {
            console.log("ERROR: Property missing from constructor");
        }
    }

    getUsername() {
        return this.username;
    }

    getPassword() {
        return this.password;
    }

    getEmail() {
        return this.email
    }

    getProfile() {
        return this.profile
    }

    getUserOptions() {
        return {
            username: this.getUsername(),
            email: this.getEmail(),
            password: this.getPassword(),
            profile: this.getProfile()
        }
    }

}

export class RawThemeCustomizer {
    constructor(rawTheme) {
        this.rawTheme = rawTheme;
    }

    customizePalette(paletteObj, paletteK, paletteV) {
        // pure function, copy obj and return a new mutated instance instead
        let _paletteObj = paletteObj;

        // prepare _paletteObj
        if (typeof paletteK === 'object' && typeof paletteV == 'undefined') {
            _paletteObj = _.extend({}, _paletteObj, paletteK);
        }

        if (typeof paletteK !== 'undefined' && typeof paletteV !== 'undefined') {
            if (this.getRawTheme().hasOwnProperty('palette')) {
                for (var key in _paletteObj) {
                    if (key === paletteK) {
                        delete _paletteObj[paletteK];
                        _paletteObj[paletteK] = paletteV;
                        break;
                    }
                }
            }
        }

        // keep the function pure <3
        let _rawTheme = this.getRawTheme();
        delete _rawTheme['palette'];
        _rawTheme['palette'] = _paletteObj;
        return _rawTheme;

    }

    getRawTheme() {
        return this.rawTheme;
    }
}

