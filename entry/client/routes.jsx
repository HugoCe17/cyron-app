import { Route, browserHistory } from 'react-router';
import cyronRoutes from 'CyronApp/client/CyronRoutes'

ReactRouterSSR.Run(
    <Route>
        {cyronRoutes}
    </Route>, {
        props: {
            onUpdate() {
                // Notify the page has been changed to Google Analytics
                //ga('send', 'pageview');
            }
        }
    }, {
        preRender: (req, res) => {
            global.navigator = {
                userAgent: req.headers['user-agent']
            };
        }
    }
);
