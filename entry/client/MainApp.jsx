import { Component, PropTypes } from 'react';
import Helmet from 'react-helmet'

injectTapEventPlugin();

export default class MainApp extends Component {
    static propTypes = {
        children: PropTypes.any.isRequired
    };

    render() {
        return (
            <div>
                <Helmet
                    title="Cyron's, LLC"
                    meta={[
                        { name: "apple-mobile-web-app-capable", content:"yes"},
                        { name: 'description', content: 'Cyron\'s Landing Page'},
                        { name:"apple-mobile-web-app-status-bar-style", content: "black-translucent"},
                        { name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0'}
                    ]}
                    link={[
                        {href: "https://fonts.googleapis.com/icon?family=Material+Icons", rel: "stylesheet"},
                        {"rel":'stylesheet', "href":'https://fonts.googleapis.com/css?family=Roboto', "type":'text/css'},
                        {"rel": "stylesheet", "href": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css", "integrity": "sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7", "crossorigin": "anonymous"},
                        {"rel": "stylesheet", "href": "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"}
                    ]}
                    script={[
                        {"href": "https://maxcd2n.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js", "integrity": "sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS", "crossorigin": "anonymous" }
                    ]}
                />
                {this.props.children}
            </div>
        );
    }
}